package com.hyz792901324.runjar;

import cn.hutool.core.io.FileUtil;
import cn.hutool.core.io.IoUtil;
import cn.hutool.core.io.LineHandler;
import cn.hutool.core.util.RuntimeUtil;
import cn.hutool.system.SystemUtil;

import javax.management.MBeanServerConnection;
import javax.management.remote.JMXConnector;
import javax.management.remote.JMXConnectorFactory;
import javax.management.remote.JMXServiceURL;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.management.ManagementFactory;
import java.lang.management.RuntimeMXBean;
import java.lang.management.ThreadMXBean;

/**
 * @author hyz
 * @date 2019/11/27 11:30
 */
public class TestRun {
    public static void main(String[] args) throws InterruptedException {
        Thread.currentThread().setName("hahahah");
        int jmxPort = 8997;
        String options =
                " -Djava.rmi.server.hostname=127.0.0.1 " +
                " -Dcom.sun.management.jmxremote " +
                " -Dcom.sun.management.jmxremote.port="+jmxPort+" " +
                " -Dcom.sun.management.jmxremote.authenticate=false " +
                " -Dcom.sun.management.jmxremote.ssl=false " +
                " -jar F:\\TestRun\\TestRun.jar ";
//        options = "";

        Process process = RuntimeUtil.exec("java "+options);
        InputStream in = process.getInputStream();
        LineHandler lineHandler = new LineHandler() {
            @Override
            public void handle(String s) {
                System.out.println(s);


//                if(process.isAlive()){
//                    int i = process.exitValue();
//                    System.out.println(i);
//                }
            }
        };
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                IoUtil.readUtf8Lines(in, lineHandler);


            }
        });
//        thread.join();
        thread.start();



//        Thread thread1 = new Thread(new Runnable() {
//            @Override
//            public void run() {
//                try {
//                    Thread.sleep(10000);
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }
//                process.destroy();
//            }
//        });
//        thread1.start();
        String  ip = "127.0.0.1";
        int port = jmxPort;
        String jmxUrl = "service:jmx:rmi:///jndi/rmi://" + ip + ":" + port+ "/jmxrmi";

    }
}
