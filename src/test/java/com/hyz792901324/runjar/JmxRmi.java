package com.hyz792901324.runjar;

import javax.management.MBeanServerConnection;
import javax.management.ObjectInstance;
import javax.management.ObjectName;
import javax.management.remote.JMXConnector;
import javax.management.remote.JMXConnectorFactory;
import javax.management.remote.JMXServiceURL;
import java.lang.management.*;
import java.text.SimpleDateFormat;
import java.util.*;

public class JmxRmi {
    /** 
     * @param args 
     */ 
    public static void main(String[] args) { 
        try {
            String  ip = "127.0.0.1";
            int port = 8997;
            String jmxURL = "service:jmx:rmi:///jndi/rmi://" + ip + ":" + port+ "/jmxrmi";
//            String jmxURL = "service:jmx:rmi:///jndi/rmi://127.0.0.1:8996/jmxrmi";//tomcat jmx url
            JMXServiceURL serviceURL = new JMXServiceURL(jmxURL);
 
            Map map = new HashMap(); 
            String[] credentials = new String[] { "monitorRole", "QED" }; 
            map.put("jmx.remote.credentials", credentials); 
            JMXConnector connector = JMXConnectorFactory.connect(serviceURL, map); 
            MBeanServerConnection mbsc = connector.getMBeanServerConnection();

            MemoryMXBean mmxb = ManagementFactory.newPlatformMXBeanProxy(mbsc,"java.lang:type=Memory",
				                MemoryMXBean.class);

            RuntimeMXBean rmxb = ManagementFactory.newPlatformMXBeanProxy(mbsc,"java.lang:type=Runtime",
                    RuntimeMXBean.class);

            ThreadMXBean thxb = ManagementFactory.newPlatformMXBeanProxy(mbsc,"java.lang:type=Threading",
                    ThreadMXBean.class);

            OperatingSystemMXBean osxb = ManagementFactory.newPlatformMXBeanProxy(mbsc,"java.lang:type=OperatingSystem",
                    OperatingSystemMXBean.class);

            System.out.println("操作系统"+osxb.getName()+" "+osxb.getVersion() +" " +  osxb.getArch());

            System.out.println("ObjectName="+thxb.getObjectName());
            System.out.println("仍活动的线程总数="+thxb.getThreadCount());
            System.out.println("峰值="+thxb.getPeakThreadCount());
            System.out.println("线程总数（被创建并执行过的线程总数）="+thxb.getTotalStartedThreadCount());
            System.out.println("当初仍活动的守护线程（daemonThread）总数="+thxb.getDaemonThreadCount());



//            //端口最好是动态取得
//            ObjectName threadObjName = new ObjectName("Catalina:type=ThreadPool,name=http-8089");
//            MBeanInfo mbInfo = mbsc.getMBeanInfo(threadObjName);
//
//            String attrName = "currentThreadCount";//tomcat的线程数对应的属性值
//            MBeanAttributeInfo[] mbAttributes = mbInfo.getAttributes();
//            System.out.println("currentThreadCount:" + mbsc.getAttribute(threadObjName, attrName));
//
            //heap   
            for (int j = 0; j < mbsc.getDomains().length; j++) { 
                System.out.println("###########" + mbsc.getDomains()[j]); 
            } 
            Set MBeanset = mbsc.queryMBeans(null, null); 
            System.out.println("MBeanset.size() : " + MBeanset.size()); 
            Iterator MBeansetIterator = MBeanset.iterator(); 
            while (MBeansetIterator.hasNext()) { 
                ObjectInstance objectInstance = (ObjectInstance) MBeansetIterator.next(); 
                ObjectName objectName = objectInstance.getObjectName(); 
                String canonicalName = objectName.getCanonicalName(); 
                System.out.println("canonicalName : " + canonicalName); 
                if (canonicalName.equals("Catalina:host=localhost,type=Cluster")) { 
                    // Get details of cluster MBeans   
                    System.out.println("Cluster MBeans Details:"); 
                    System.out.println("========================================="); 
                    //getMBeansDetails(canonicalName);   
                    String canonicalKeyPropList = objectName.getCanonicalKeyPropertyListString(); 
                } 
            } 
            //------------------------- system ----------------------   
            ObjectName runtimeObjName = new ObjectName("java.lang:type=Runtime"); 
            System.out.println("厂商:" + rmxb.getVmVendor());
            System.out.println("程序:" + rmxb.getVmName());
            System.out.println("版本:" + rmxb.getVmVersion());

            String[] arr = rmxb.getName().split("@");
            System.out.println("进程号:" + arr[0]);
            System.out.println("用户:" + arr[1]);
            System.out.println("java 版本:"+rmxb.getSpecVersion());
            System.out.println("java 版本:"+rmxb.getSpecName());

            Date starttime = new Date(rmxb.getStartTime());
            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); 
            System.out.println("启动时间:" + df.format(starttime)); 
 
            Long timespan = (Long) mbsc.getAttribute(runtimeObjName, "Uptime"); 
            System.out.println("连续工作时间:" + JmxRmi.formatTimeSpan(timespan));
            //------------------------ JVM -------------------------   
            //堆使用率   
            MemoryUsage heapMemoryUsage = mmxb.getHeapMemoryUsage();
            long maxMemory = heapMemoryUsage.getMax();//堆最大   
            long commitMemory = heapMemoryUsage.getCommitted();//堆当前分配   
            long usedMemory = heapMemoryUsage.getUsed();
            System.out.println("最大内存:"+maxMemory/1024/1024);
            System.out.println("已分配内存:"+commitMemory/1024/1024);
            System.out.println("已使用内存:"+usedMemory/1024/1024);
            System.out.println("内存使用率:" + (double) usedMemory * 100 / commitMemory + "%");//堆使用率
 
            MemoryUsage nonheapMemoryUsage = mmxb.getNonHeapMemoryUsage();
            long noncommitMemory = nonheapMemoryUsage.getCommitted(); 
            long nonusedMemory = heapMemoryUsage.getUsed();
            System.out.println("未使用内存:"+nonusedMemory/1024/1024);
            System.out.println("未提交内存:"+noncommitMemory/1024/1024);

//            System.out.println("nonheap:" + (double) nonusedMemory * 100 / noncommitMemory + "%");
 
//            ObjectName permObjName = new ObjectName("java.lang:type=MemoryPool,name=Perm Gen");
//            MemoryUsage permGenUsage = MemoryUsage.from((CompositeDataSupport) mbsc.getAttribute(permObjName, "Usage"));
//            long committed = permGenUsage.getCommitted();//持久堆大小
//            long used = heapMemoryUsage.getUsed();//
//            System.out.println("perm gen:" + (double) used * 100 / committed + "%");//持久堆使用率

//            ObjectName objectName = new ObjectName("java.lang:type=Memory");
//            MemoryMXBean memoryMXBean = (MemoryMXBean) mbsc.getAttribute(objectName, null);

            //-------------------- Session ---------------    
//            ObjectName managerObjName = new ObjectName("Catalina:type=Manager,*");
//            Set<ObjectName> s = mbsc.queryNames(managerObjName, null);
//            for (ObjectName obj : s) {
//                System.out.println("应用名:" + obj.getKeyProperty("path"));
//                ObjectName objname = new ObjectName(obj.getCanonicalName());
//                System.out.println("最大会话数:" + mbsc.getAttribute(objname, "maxActiveSessions"));
//                System.out.println("会话数:" + mbsc.getAttribute(objname, "activeSessions"));
//                System.out.println("活动会话数:" + mbsc.getAttribute(objname, "sessionCounter"));
//            }
 
            //----------------- Thread Pool ----------------   
//            ObjectName threadpoolObjName = new ObjectName("Catalina:type=ThreadPool,*");
//            Set<ObjectName> s2 = mbsc.queryNames(threadpoolObjName, null);
//            for (ObjectName obj : s2) {
//                System.out.println("端口名:" + obj.getKeyProperty("name"));
//                ObjectName objname = new ObjectName(obj.getCanonicalName());
//                System.out.println("最大线程数:" + mbsc.getAttribute(objname, "maxThreads"));
//                System.out.println("当前线程数:" + mbsc.getAttribute(objname, "currentThreadCount"));
//                System.out.println("繁忙线程数:" + mbsc.getAttribute(objname, "currentThreadsBusy"));
//            }
        } catch (Exception e) {
            e.printStackTrace(); 
        } 
    } 
 
    public static String formatTimeSpan(long span) { 
        long minseconds = span % 1000; 
 
        span = span / 1000; 
        long seconds = span % 60; 
 
        span = span / 60; 
        long mins = span % 60; 
 
        span = span / 60; 
        long hours = span % 24; 
 
        span = span / 24; 
        long days = span; 
        return (new Formatter()).format("%1$d天 %2$02d:%3$02d:%4$02d.%5$03d", days, hours, mins, seconds, minseconds) 
                .toString(); 
    } 
 
}  