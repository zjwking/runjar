package com.hyz792901324.runjar;

import cn.hutool.core.io.IoUtil;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.stream.Stream;

/**
 * @author hyz
 * @date 2019/11/29 17:56
 */
public class TestReader {
    public static void main(String[] args) throws Exception {
        File file = new File("F:\\project\\logs\\Test\\a5b8cfb88a62468484e16958c5fb8d1b-20191129.log");
        BufferedReader reader =
                IoUtil.getReader(new FileInputStream(file),
                        "UTF-8");
        long count = reader.lines().count();
        reader.skip(((count-100)/count)*file.length());
        System.out.println(reader.readLine());
        reader.close();
    }
}
