/**
 编辑api
 **/
layui.define(['element', 'layer', 'form'], function (exports) {
    var layer = layui.layer
        , form = layui.form;

    var element = layui.element;

    form.on('submit(form)', function(data){
        //layer.msg(JSON.stringify(data.field));
        util.post("admin/project/save",data.field,function(res){
            layer.msg("保存成功!");
            parent.callback();
        });
        return false;
    });

    exports('add', {}); //注意，这里是模块输出的核心，模块名必须和use时的模块名一致
});