/**
 项目JS主入口
 API管理
 **/
layui.define(['element', 'layer', 'form', 'table','tableChild'], function (exports) {
    var layer = layui.layer
        , form = layui.form,
        tableChild = layui.tableChild;

    var element = layui.element;

    var table = layui.table; //很重要

    //方法级渲染
    var dicTable = util.table.render({
        elem: '#table'
        ,url: 'admin/project/list',
        queryForm:'queryForm'
        ,cols: [[
            {title: '#', width: 50, children:function(row){
                    var arr = [
                        {
                            title: '实例'
                            ,height: 300
                            ,data: row.instances
                            ,page: false
                            ,cols: [[
                                {field: 'version', title: '版本号', width: 200, sort: true, filter: true,templet:"#instance-version"},
                                {field: 'run', title: '状态', width: 200, sort: true, filter: true,templet:"#instance-status"},
                                {field: 'pid', title: '进程号pid', width: 100, sort: true,templet:"#instance-pid"},
                                {title: '操作', minWidth: 156, templet: '#instance-tool'}
                            ]]
                            ,done: function () {
                                tableChild.render(this);
                            }
                        },{
                            title: '版本'
                            ,height: 300
                            ,data: row.versions.reverse()
                            ,page: false
                            ,cols: [[
                                {field: 'version', title: '版本号', width: 200, sort: true, filter: true},
                                {field: 'jarPath', title: 'jar名称', width: 200, sort: true, filter: true},
                                {title: '操作', minWidth: 156, templet: '#version-tool'}
                            ]]
                            ,done: function () {
                                tableChild.render(this);
                            }
                        }
                    ];
                    return arr;
                }
            },
            {field:'name', title: '项目名称', width:200},
            {field:'status', title: '状态', width:100},
            {field:'config', title: '项目配置', width:500},
            {field:'id',title:'操作', templet: '#table-tool'}
        ]]
        ,id: 'table'
        ,page: false
        ,height: 'full-90',
        rowClick :function(obj){
            curObj = obj;
        },
        done:function(me){
            curObj = null;
            tableChild.render(me);
        }
    });
    var curObj = null;
    edit = function (id){
        //layer.msg(id);
        openEdit(id);
    };
    editUpload = function (id){
        //layer.msg(id);
        openEditUpload(id);
    };
    addInstance = function (projectId){
        //layer.msg(id);
        openEditInstance(projectId);
    };
    editInstance = function (id){
        //layer.msg(id);
        openEditInstance(null,id);
    };
    logInstance = function (id){
        //layer.msg(id);
        var title ="查看日志";
        var url = "admin/projectInstance/log/" + id;
        editWin = util.open(title,url,$(window).width()*0.8,$(window).height()*0.8);
    };
    add = function (id){
        //layer.msg(id);
        openEdit();
    };

    delVersion = function(id,name){
        util.confirm('确定删除版本[<span style="color: red;">'+name+']</span>吗?',function(){
            util.post("admin/projectVersion/delete/"+id,{},function(){
                callback();
                layer.msg("删除成功!");
            });
        });

    };

    delInstance = function(id,name){
        util.confirm('确定删除实例吗?',function(){
            util.post("admin/projectInstance/delete/"+id,{},function(){
                callback();
                layer.msg("删除成功!");
            });
        });
    };
    startInstance = function(id,name){
        util.confirm('确定启动实例吗?',function(){
            util.post("admin/projectInstance/start/"+id,{},function(){
                callback();
                layer.msg("启动成功!");
            });
        });
    };
    stopInstance = function(id,name){
        util.confirm('确定停止实例吗?',function(){
            util.post("admin/projectInstance/stop/"+id,{},function(){
                callback();
                layer.msg("停止成功!");
            });
        });
    };

    var editWin = null;

    function openEdit(id){
        var title = (id == null ? "添加" :"编辑") +"项目";
        var url = "admin/project/add" + (id != null ? "?id="+id : "");
        editWin = util.open(title,url,500,500);
    }
    function openEditUpload(id){
        var title = (id == null ? "添加" :"编辑") +"项目版本";
        var url = "admin/projectVersion/add" + (id != null ? "?projectId="+id : "");
        editWin = util.open(title,url,500,500);
    }
    function openEditInstance(projectId,id){
        var title = (id == null ? "添加" :"编辑") +"实例";
        var url = "admin/projectInstance/add" + (projectId != null ? "?projectId="+projectId : "?id="+id);
        editWin = util.open(title,url,500,500);
    }

    callback = function(){
        if(editWin){
            editWin.close();
            editWin = null;
        }
        dicTable.query();
    }

    exports('index', {}); //注意，这里是模块输出的核心，模块名必须和use时的模块名一致
});