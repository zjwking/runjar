/**
  项目JS主入口
  以依赖layui的layer和form模块为例
**/    
layui.define(['element','layer', 'form'], function(exports){
  var layer = layui.layer
  ,form = layui.form;
  
  //layer.msg('Hello World SS');
  

  var element = layui.element;
  /*
  element.tabAdd('demo', {
	  title: '选项卡的标题'
	  ,href: 'test.html' //支持传入html
	  ,id: '选项卡标题的lay-id属性值'
	});             
	*/

    element.on('nav(menu)', function(elem){
        //console.info(elem); //得到当前点击的DOM对象
        //$(elem);
        //console.info($(elem).attr("lay-filter"));
        var id = $(elem).attr("lay-filter");
        if(id == null || id == ""){
          id = new Date().getTime();
          $(elem).attr("lay-filter",id);
        }

        if($("*[lay-id=menu-"+id+"]").size() > 0){
            element.tabChange("main-tab", "menu-"+id);
        }else{
            var _href = $(elem).attr("_href");
            if(_href == null){
              return;
            }
            element.tabAdd('main-tab', {
                title: $(elem).text()
                ,content: '<iframe src="'+$(elem).attr("_href")+'" class="layadmin-iframe layui-side-scroll" frameborder="0"></iframe>'  //支持传入html
                ,id: "menu-"+id
            });
            element.tabChange("main-tab", "menu-"+id);
        };
    });
    //console.info($("#menu .layui-nav-child a").eq(0));
    $("#menu .layui-nav-child a").eq(0).click();
    exports('index', {}); //注意，这里是模块输出的核心，模块名必须和use时的模块名一致
});