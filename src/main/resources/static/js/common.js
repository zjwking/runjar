util = {
    constants:{
        statusCode:{
            ok :0
        }
    },
    table :{
        render :function(config){
            var _done = config.done;
            config.done = function(){
                var me = this;
                if(_done){
                    _done(me,arguments);
                }
                allRowClick();
            };
            if(config.initSort) {
                if (!config.where) config.where = {};
                config.where.sortField = config.initSort.field;
                config.where.sortType = config.initSort.type;
            }
            var sort = config.initSort;

            function allRowClick(){
                $("#"+config.id).next().find("tbody tr").click(function(){
                    var id = $(this).find("td[data-field=id]").attr("data-content");
                    if(id == null || id == ""){
                        return;
                    }
                    var items = layui.table.cache[config.id];
                    for(var i =0;i<items.length;i++){
                        if(items[i].id == id){
                            if(config.rowClick){
                                config.rowClick(items[i]);
                            }
                            break;
                        }
                    }
                    if(config.rowClick){
                        $(this).parent().find(".trSelect").removeClass("trSelect");
                        $(this).addClass("trSelect");
                    }
                });
            };

            var query = function(){
                if(config["queryForm"]){
                    var obj = util.getFormValues($("#"+config["queryForm"]));
                    if(sort != null && sort.type != ""){
                        obj.sortField = sort.field;
                        obj.sortType  = sort.type;
                    }
                    for(var i in fixedWhere){
                        obj[i] = fixedWhere[i];
                    }
                    layui.table.reload(config.id,{
                        page: {
                            curr: 1 //重新从第 1 页开始
                        },
                        where: obj,
                        initSort:sort
                    });
                }
            }
            var fixedWhere = config.where || {};
            var uiTable = layui.table.render(config);


            layui.table.on("sort("+config.id+")",function(obj){
                sort = obj;
                query();
            });
            return {
                query:query,
                uiTable:uiTable,
                setFixedWhere :function(where){
                    fixedWhere = where || {};
                }
            };
        }
    },
    confirm:function(msg,callback){
        var index  = layer.confirm(msg, {icon: 3, title:'提示'}, function(index){
            //do something
            layer.close(index);

            if(callback){ callback()}
        });
    },
    alert:function(msg){
        var index  = layer.alert(msg, {icon: 2, title:'提示'}, function(index){
            //do something
            layer.close(index);
        });
    },
    open :function(title,url,width,height,full){
        var index = layer.open({
            title:title,
            type: 2,
            content: url //这里content是一个URL，如果你不想让iframe出现滚动条，你还可以content: ['http://sentsin.com', 'no']
        });
        if(full){
            layer.full(index);
        }else{
            layer.style(index, {
                top:'30px',
                left: ($(window).width()-parseInt(width))/2-20,
                height:height,
                width:width
            });
        }
        return {
            close :function(){
                layer.close(index);
            },
            full :function(){
                layer.full(index);
            }
        }
    },
    ajax :function(url,callback,type,data){
        //var ld = layer.msg('加载中...', { icon: 16, shade: [0.5, '#f5f5f5'], scrollbar: false, offset: '50%', time: 300000 });
        var ld = layui.layer.load();
        $.ajax({
            url:url,
            type:type,
            data:data,
            success :function(data){
                layer.close(ld);
                try{
                    data = eval("("+data+")");
                }catch (e){
                }
                if(util.constants.statusCode.ok == data.code){
                    callback(data.data);
                }else {
                    layer.alert(data.msg);
                }
            },
            error:function(){
                layer.close(ld);
                layer.alert("加载失败");
            }
        });
    },
    ajaxFileUpload :function(url,callback,type,data,fileID){
        //var ld = layer.msg('加载中...', { icon: 16, shade: [0.5, '#f5f5f5'], scrollbar: false, offset: '50%', time: 300000 });
        var ld = layui.layer.load();
        $.ajaxFileUpload({
            url:url,
            type:type,
            fileElementId : fileID,
            secureuri : false,
            data:data,
            success :function(data){
                layer.close(ld);
                data = $(data.body).text();
                try{
                    data = eval("("+data+")");
                }catch (e){
                }
                if(util.constants.statusCode.ok == data.code){
                    callback(data.data);
                }else {
                    layer.alert(data.msg);
                }
            },
            error:function(){
                layer.close(ld);
                layer.alert("加载失败");
            }
        });
    },
    post:function(url,data,callback){
        this.ajax(url,callback,"POST",data);
    },
    get:function(url,callback) {
        this.ajax(url, callback, "GET");
    },
    selectTree:function(url,value,callback,textField,idField,parentIdField){
        idField = idField || 'id';
        parentIdField = parentIdField || 'parentId';
        this.get(url,function(data){
            data = util.arrayToTree(data,textField,idField,parentIdField);
            layui.use('tree', function(){

                var index = layer.open({
                    title:"请选择",
                    type: 1,
                    offset:"auto",
                    content: "<ul id='select_tree'></ul>" //这里content是一个URL，如果你不想让iframe出现滚动条，你还可以content: ['http://sentsin.com', 'no']
                });
                layer.style(index, {
                    top:'30px',
                    left: ($(window).width()-200)/2-20,
                    width: "200px",
                    height:"300px"
                });


                layui.tree({
                    elem:"#select_tree",
                    nodes:data,
                    click:function(node){
                        callback(node);
                        layer.close(index);
                    }
                });

            });

        });
    },
    arrayToTree : function(data,textField,idField,parentIdField) {
        var treeMap = {};
        var rootIds = [];
        var childIds = {};

        for(var i in data){
            //if(data[i][parentIdField] == "") data[i][parentIdField] = null;
            treeMap[data[i][idField]]=data[i];
            data[i].name = data[i][textField];
        }

        for(var i in data){
            //没有父节点 表示 是根节点   父节点 不存在集合中 也属于 根节点
            var t = data[i];
            if(t[parentIdField] == null || t[parentIdField] == "" || !treeMap[t[parentIdField]]){
                rootIds.push(t[idField]);
            }else{
                if(!childIds[t[parentIdField]]){
                    childIds[t[parentIdField]]=[];
                }
                childIds[t[parentIdField]].push(t[idField]);
            }
        }

        var roots = [];
        for(var i in rootIds){
            var root = treeMap[rootIds[i]];
            util._toTree(root, treeMap, childIds,idField);
            roots.push(root);
        }
        return roots;
    },
    _toTree :function(root,treeMap,childIds,idField){
        var list = childIds[root[idField]];
        if(list == null || list.length == 0){
            return;
        }
        for(var i in list){
            var childMenu = treeMap[list[i]];
            if(!root.children){
                root.children = [];
            }
            root.children.push(childMenu);
            childMenu.parent = root;
            util._toTree(childMenu, treeMap, childIds,idField);
        }
    },
    getFormValues : function(form) {
        var selects = form.find("select");
        var inputs = form.find("input");
        var textareas = form.find("textarea");
        var values = {};

        var names = {};
        var arr = [];
        for ( var i = 0; i < selects.size(); i++) {
            var select = $(selects.get(i));
            var name = select.attr("name");
            // 如果有多个下拉框重名 应该将参数变成数组
            // 需要遍历2遍
            if (util.isNull(name)) {
                continue;
            }
            if (!names[name]) {
                names[name] = 0;
            }
            names[name]++;
            arr.push(select);
        }
        for ( var i = 0; i < textareas.size(); i++) {
            var textarea = $(textareas.get(i));
            var name = textarea.attr("name");
            // 如果有多个下拉框重名 应该将参数变成数组
            // 需要遍历2遍
            if (util.isNull(name)) {
                continue;
            }
            if (!names[name]) {
                names[name] = 0;
            }
            names[name]++;
            arr.push(textarea);
        }
        // 遍历 input元素
        for ( var i = 0; i < inputs.size(); i++) {
            var input = $(inputs.get(i));
            var type = input.attr("type");
            var name = input.attr("name");
            var value = input.val();
            if (util.isNull(name)) {
                continue;
            }
            if(value && input.attr("xtype") == "money"){
                value = value.replace(/[^0-9\.]/g,"");
            }

            if (type == "radio") {
                // 单选框
                // 一定是 单值的
                var checked = input.is(":checked");
                if (checked) {
                    values[name] = value;
                }
            } else if (type == "checkbox") {
                // 复选框
                // 一定是多值的
                var checked = input.is(":checked");
                if (!values[name]) {
                    values[name] = [];
                }
                if (checked) {
                    values[name].push(value);
                }else{
                    var unValue = input.attr("unValue");
                    if(unValue != "" && unValue != null){
                        values[name].push(unValue);
                    }
                }
            } else {
                // 其他
                // 可能是单值,也可能是多值
                if (!names[name]) {
                    names[name] = 0;
                }
                names[name]++;
                arr.push(input);
            }
        }

        // 下面遍历 那些不确定是一个值还是多个值的元素
        for ( var i = 0; i < arr.length; i++) {
            var input_select = arr[i];
            var value = input_select.val();
            var name = input_select.attr("name");
            if (names[name] > 1) {
                // 说明是数组元素 多值
                if (!values[name]) {
                    values[name] = [];
                }
                values[name].push(value);
            } else {
                values[name] = value;
            }
        }
        return values;
    },
    isNull :function(value){
        return value == null || value == "";
    }
};