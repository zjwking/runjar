package com.hyz792901324.runjar;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
@SpringBootApplication
public class RunjarApplication {

	public static void main(String[] args) {
		SpringApplication.run(RunjarApplication.class, args);
	}

}
