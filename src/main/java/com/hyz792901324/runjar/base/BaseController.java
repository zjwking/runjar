package com.hyz792901324.runjar.base;

import com.hyz792901324.runjar.util.ReflectUtil;
import com.hyz792901324.runjar.util.StringUtil;
import com.hyz792901324.runjar.util.Util;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Controller
public class BaseController {
    protected Logger logger = LoggerFactory.getLogger(getClass());


    protected HttpServletRequest getRequest() {
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
        try {
            request.setCharacterEncoding("UTF-8");
        }catch (Exception e){
        }
        return request;
    }

    protected HttpServletResponse getResponse(){
        HttpServletRequest request = getRequest();
        return ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getResponse();
    }

    protected HttpSession getSession() {
        return getRequest().getSession();
    }

    protected String getStr(String name){
        String value = getRequest().getParameter(name);
        return value;
    }

    protected Integer getInt(String name){
        return getObj(name,Integer.class);
    }

    protected Long getLong(String name){
        return getObj(name,Long.class);
    }

    protected Date getDate(String name){
        return getObj(name,Date.class);
    }

    protected <N> N getObj(String name,Class<N> clz){
        return StringUtil.toObj(getStr(name),clz);
    }


    protected String[] getStrs(String name){
        String[] values = getRequest().getParameterValues(name);
        if (values == null) {
            values = getRequest().getParameterValues(name + "[]");
        }
        if (values == null) {
            return new String[] {};
        }
        return values;
    }

    protected Integer[] getInts(String name){
        return getArr(name,Integer.class);
    }

    protected Date[] getDates(String name){
        return getArr(name,Date.class);
    }

    protected <N> N[] getArr(String name,Class<N> clz){
        String[] strs = getStrs(name);
        N[] arr = (N[]) Array.newInstance(clz,strs.length);
        for(int i=0;i<strs.length;i++){
            arr[i] = StringUtil.toObj(strs[i],clz);
        }
        return arr;
    }

    protected void setValues(Object obj){
        Map<String, Field> fieldsMap = ReflectUtil.getFieldsMap(obj.getClass(), Object.class);
        Map<String, String> params = getParams();
        for(Map.Entry<String,String> en:params.entrySet()){
            Field field = fieldsMap.get(en.getKey());
            if(field == null) continue;
            Class<?> type = field.getType();
            if(en.getValue() == null){
                ReflectUtil.reflectSetField(field,obj,null);
                continue;
            }
            Object value = StringUtil.toObj(en.getValue(), type);
            ReflectUtil.reflectSetField(field,obj,value);

            try{
                obj.getClass().getMethod("set"+StringUtil.getMethodNameByFieldName(field.getName()),field.getType()).invoke(obj,value);
            }catch (Exception e){

            }
        }
    }

    protected String _index(){
        return getPackageName()+"/index";
    }

    protected String _add(){
        return getPackageName()+"/add";
    }



    protected String getPackageName(){
        RequestMapping mapping = getClass().getAnnotation(RequestMapping.class);
        return mapping == null || mapping.value() == null || mapping.value().length==0 ? "" :mapping.value()[0].substring(1);
    }

    protected Integer getInt(String name,Integer defaultValue){
        return Util.get(getInt(name),defaultValue);
    }

    protected Map<String, String> getParams(){
        Map<String, String[]> parameterMap = getRequest().getParameterMap();
        Map<String, String> map = new HashMap<String, String>();
        for(Map.Entry<String, String[]> en:parameterMap.entrySet()){
            map.put(en.getKey(), getStr(en.getKey()));
        }
        return map;
    }


}
