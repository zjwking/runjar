package com.hyz792901324.runjar.admin.model;

import java.lang.management.MemoryMXBean;
import java.lang.management.OperatingSystemMXBean;
import java.lang.management.RuntimeMXBean;
import java.lang.management.ThreadMXBean;

/**
 * @author hyz
 * @date 2019/11/29 15:41
 */
public class JmxRmiInfo {
    private MemoryMXBean memoryMXBean;
    private RuntimeMXBean runtimeMXBean;
    private ThreadMXBean threadMXBean;
    private OperatingSystemMXBean operatingSystemMXBean;

    public MemoryMXBean getMemoryMXBean() {
        return memoryMXBean;
    }

    public void setMemoryMXBean(MemoryMXBean memoryMXBean) {
        this.memoryMXBean = memoryMXBean;
    }

    public RuntimeMXBean getRuntimeMXBean() {
        return runtimeMXBean;
    }

    public void setRuntimeMXBean(RuntimeMXBean runtimeMXBean) {
        this.runtimeMXBean = runtimeMXBean;
    }

    public ThreadMXBean getThreadMXBean() {
        return threadMXBean;
    }

    public void setThreadMXBean(ThreadMXBean threadMXBean) {
        this.threadMXBean = threadMXBean;
    }

    public OperatingSystemMXBean getOperatingSystemMXBean() {
        return operatingSystemMXBean;
    }

    public void setOperatingSystemMXBean(OperatingSystemMXBean operatingSystemMXBean) {
        this.operatingSystemMXBean = operatingSystemMXBean;
    }

    public String getPid(){
        String[] arr = runtimeMXBean.getName().split("@");
        return arr[0];
    }
}
