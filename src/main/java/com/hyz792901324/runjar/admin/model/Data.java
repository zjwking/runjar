package com.hyz792901324.runjar.admin.model;

import java.util.ArrayList;
import java.util.List;

/**
 * @author hyz
 * @date 2019/11/28 11:31
 */
public class Data {
    private List<Project> projects = new ArrayList<>();

    public List<Project> getProjects() {
        return projects;
    }

    public void setProjects(List<Project> projects) {
        this.projects = projects;
    }
}
