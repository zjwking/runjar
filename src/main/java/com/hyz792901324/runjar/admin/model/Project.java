package com.hyz792901324.runjar.admin.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * @author hyz
 * @date 2019/11/28 11:09
 */
public class Project {
    //id
    private String id;
    //项目名称
    private String name;

    //版本列表
    private List<ProjectVersion> versions = new ArrayList<>();

    //项目运行实例
    private List<ProjectInstance> instances = new ArrayList<>();

    public String getName() {
        return name;
    }

    public void setInstances(List<ProjectInstance> instances) {
        this.instances = instances;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setVersions(List<ProjectVersion> versions) {
        this.versions = versions;
    }

    public List<ProjectInstance> getInstances() {
        return instances;
    }

    public List<ProjectVersion> getVersions() {
        return versions;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Project project = (Project) o;
        return Objects.equals(id, project.id);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id);
    }
}
