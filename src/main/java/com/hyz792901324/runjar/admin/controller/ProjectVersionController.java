package com.hyz792901324.runjar.admin.controller;

import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.StrUtil;
import com.hyz792901324.runjar.admin.model.Project;
import com.hyz792901324.runjar.admin.model.ProjectVersion;
import com.hyz792901324.runjar.base.BaseController;
import com.hyz792901324.runjar.data.DataService;
import com.hyz792901324.runjar.rest.Resp;
import com.hyz792901324.runjar.util.DateUtil;
import com.hyz792901324.runjar.util.RespUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.Date;

/**
 * @author hyz
 * @date 2019/11/28 16:56
 */
@Controller
@RequestMapping("/admin/projectVersion")
public class ProjectVersionController extends BaseController{

    @Autowired
    private DataService dataService;

    @RequestMapping("/add")
    public String add(Model model){
        model.addAttribute("projectId",super.getStr("projectId"));
        return super._add();
    }

    @RequestMapping("/save")
    @ResponseBody
    public Resp save(@RequestParam("file") MultipartFile file,
                     @RequestParam("version") String version,
                     @RequestParam("projectId") String projectId) throws IOException {
        if(file.isEmpty()){
            return RespUtil.fail("请选择文件");
        }
        if(StrUtil.isBlank(version)){
            version = DateUtil.toStr(new Date(),"yyyyMMddHHmmss");
        }
        Project project = dataService.findProjectById(projectId);
        ProjectVersion projectVersion = new ProjectVersion();
        projectVersion.setId(IdUtil.fastSimpleUUID());
        projectVersion.setVersion(version);

        String folder = project.getName()+"/"+version+"/";
        projectVersion.setJarPath(file.getOriginalFilename());
        File parent = new File(dataService.getFolder(),folder);
        parent.mkdirs();

        file.transferTo(new File(parent,projectVersion.getJarPath()));

        project.getVersions().add(projectVersion);
        dataService.saveProject(project);

        return RespUtil.success();
    }

    @RequestMapping("/delete/{id}")
    @ResponseBody
    public Resp delete(@PathVariable("id") String id){
        if(dataService.isRunVersion(id)){
            return RespUtil.fail("该版本正在运行,不能删除");
        }
        dataService.deleteVersion(id);
        return RespUtil.success();
    }

}
