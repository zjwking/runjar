package com.hyz792901324.runjar.admin.controller;

import cn.hutool.core.util.StrUtil;
import com.hyz792901324.runjar.admin.model.Project;
import com.hyz792901324.runjar.base.BaseController;
import com.hyz792901324.runjar.config.Config;
import com.hyz792901324.runjar.data.DataService;
import com.hyz792901324.runjar.rest.Resp;
import com.hyz792901324.runjar.util.RespUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author hyz
 * @date 2019/11/27 17:31
 */
@Controller
@RequestMapping("/admin/project")
public class ProjectController extends BaseController{

    @Autowired
    Config config;

    @Autowired
    private DataService dataService;

    @RequestMapping("/index")
    public String index(){
        return super._index();
    }

    @RequestMapping("/list")
    @ResponseBody
    public Resp list(){
        return RespUtil.success(dataService.list());
    }

    @RequestMapping("/add")
    public String add(Model model){
        String id = super.getStr("id");
        Project project = null;
        if(StrUtil.isNotBlank(id)){
            project = dataService.findProjectById(id);
        }else{
            project = new Project();
        }
        model.addAttribute("project",project);
        return super.getPackageName() + "/add";
    }

    @RequestMapping("/save")
    @ResponseBody
    public Resp save(){
        String id = super.getStr("id");
        String name = super.getStr("name");
        if(dataService.isRepeat(id,name)){
            return RespUtil.fail("项目名称重复");
        }
        dataService.saveProject(id,name);
        return RespUtil.success();
    }
}
