package com.hyz792901324.runjar.admin.controller;

import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.StrUtil;
import com.hyz792901324.runjar.admin.model.Project;
import com.hyz792901324.runjar.admin.model.ProjectInstance;
import com.hyz792901324.runjar.base.BaseController;
import com.hyz792901324.runjar.data.DataService;
import com.hyz792901324.runjar.rest.Resp;
import com.hyz792901324.runjar.util.RespUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author hyz
 * @date 2019/11/29 11:29
 */
@Controller
@RequestMapping("/admin/projectInstance")
public class ProjectInstanceController extends BaseController{

    @Autowired
    private DataService dataService;
    @RequestMapping("/add")
    public String add(Model model){
        String projectId = super.getStr("projectId");
        String id = super.getStr("id");
        ProjectInstance projectInstance = null;
        Project project = null;
        if(StrUtil.isNotBlank(id)){
            projectInstance =dataService.findProjectInstanceById(id);
            project = dataService.findProjectByInstanceId(id);
        }else{
            projectInstance = new ProjectInstance();
            project = dataService.findProjectById(projectId);
        }
        model.addAttribute("project",project);
        model.addAttribute("projectInstance",projectInstance);
        return super._add();
    }

    @RequestMapping("/save")
    @ResponseBody
    public Resp save(){
        String id = super.getStr("id");
        String versionId = super.getStr("versionId");
        String config = super.getStr("config");
        String projectId = super.getStr("projectId");
        ProjectInstance projectInstance = null;
        if(StrUtil.isNotBlank(id)){
            projectInstance = dataService.findProjectInstanceById(id);
            if(projectInstance.isRun()){
                return RespUtil.fail("该实例正在运行,不能修改");
            }
        }else {
            projectInstance = new ProjectInstance();
            projectInstance.setId(IdUtil.fastSimpleUUID());

            dataService.findProjectById(projectId).getInstances().add(projectInstance);
        }
        projectInstance.setConfig(config);
        if(versionId.equals("newVersion")){
            projectInstance.setNewVersion(true);
        }else{
            projectInstance.setNewVersion(false);
            projectInstance.setVersion(dataService.findVersion(versionId));
        }

        dataService.saveData();

        return RespUtil.success();
    }

    @RequestMapping("/delete/{id}")
    @ResponseBody
    public Resp delete(@PathVariable("id") String id){
        ProjectInstance projectInstance = dataService.findProjectInstanceById(id);
        if(projectInstance.isRun()){
            return RespUtil.fail("该实例正在运行,不能直接删除!");
        }
        Project project = dataService.findProjectByInstanceId(id);
        project.getInstances().remove(projectInstance);
        dataService.saveData();
        return RespUtil.success();
    }
    @RequestMapping("/start/{id}")
    @ResponseBody
    public Resp start(@PathVariable("id") String id){
        ProjectInstance projectInstance = dataService.findProjectInstanceById(id);
        if(projectInstance.isRun()){
            return RespUtil.fail("该实例正在运行,不需要重复运行!");
        }
        if(projectInstance.isNewVersion()){
            Project project = dataService.findProjectByInstanceId(id);
            if(project.getVersions().isEmpty()){
                return RespUtil.fail("没有可运行的版本");
            }
        }

        dataService.startInstance(projectInstance);
        return RespUtil.success();
    }

    @RequestMapping("/stop/{id}")
    @ResponseBody
    public Resp stop(@PathVariable("id") String id){
        ProjectInstance projectInstance = dataService.findProjectInstanceById(id);
        if(!projectInstance.isRun()){
            return RespUtil.fail("该实例没有运行,不需要停止!");
        }
        dataService.stopInstance(projectInstance);
        return RespUtil.success();
    }

    @RequestMapping("/log/{id}")
    public String log(Model model,@PathVariable("id") String id){
        model.addAttribute("id",id);
        return super.getPackageName()+"/log";
    }
}