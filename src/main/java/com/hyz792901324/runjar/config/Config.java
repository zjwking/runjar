package com.hyz792901324.runjar.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * @author hyz
 * @date 2019/11/28 9:25
 */
@Configuration
@ConfigurationProperties("project")
public class Config {
    private String path;

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }
}
