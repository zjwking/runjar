package com.hyz792901324.runjar.common;

import cn.hutool.log.StaticLog;
import com.hyz792901324.runjar.data.DataService;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

@Component
public class SpringContext implements ApplicationContextAware {

	private static ApplicationContext applicationContext;


	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		StaticLog.info("启动");
		SpringContext.applicationContext = applicationContext;

		DataService dataService = getBean(DataService.class);

		dataService.loadData();
	}

	public static <T> T getBean(Class<T> clz){
		return applicationContext.getBean(clz);
	}

	@SuppressWarnings("unchecked")
	public static <T> T getBean(String name){
		return (T) applicationContext.getBean(name);
	}
}
