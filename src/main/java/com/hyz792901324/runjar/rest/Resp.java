package com.hyz792901324.runjar.rest;

public class Resp implements java.io.Serializable{

    private static final long serialVersionUID = -917022587420104979L;
    /** 状态码 */
    private int code;
    /** 数据 */
    private Object data;
    /** 错误信息 */
    private String msg;

    /**数量*/
    private int count;

    public Resp() {
    }

    public Resp(int code, Object data, String msg) {
        this.code = code;
        this.data = data;
        this.msg = msg;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public int getCount() {
        return count;
    }
}
