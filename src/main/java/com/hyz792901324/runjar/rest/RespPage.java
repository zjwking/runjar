package com.hyz792901324.runjar.rest;

import java.util.ArrayList;
import java.util.List;

public class RespPage {
    /** 总数 */
    private int count;
    /** 总页数 */
    private int page;
    /** 每页条数 */
    private int limit;
    /** 当前页 */
    private int curPage;
    /** 明细 */
    private List<?> items = new ArrayList<>();

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getLimit() {
        return limit;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

    public int getCurPage() {
        return curPage;
    }

    public void setCurPage(int curPage) {
        this.curPage = curPage;
    }

    public List<?> getItems() {
        return items;
    }

    public void setItems(List<?> items) {
        this.items = items;
    }
}
