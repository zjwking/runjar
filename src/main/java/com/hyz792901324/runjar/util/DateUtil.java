package com.hyz792901324.runjar.util;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class DateUtil {
    public static enum Format{
        YYYY("yyyy"),
        YYYY_MM("yyyy-MM"),
        YYYY_MM_DD("yyyy-MM-dd"),
        YYYY_MM_DD_HH("yyyy-MM-dd HH"),
        YYYY_MM_DD_HH_MM("yyyy-MM-dd HH:mm"),
        YYYY_MM_DD_HH_MM_SS("yyyy-MM-dd HH:mm:ss")
        ;

        private String format;
        private Format(String format){
            this.format = format;
        }
        public String getFormat() {
            return format;
        }
    }

    public static Map<String,SimpleDateFormat> formatMap = new HashMap<>();

    public static synchronized SimpleDateFormat getDateFormat(String format){
        if(!formatMap.containsKey(format)){
            formatMap.put(format,new SimpleDateFormat(format));
        }
        return formatMap.get(format);
    }

    public static String getFormat(String str){
        if(str == null) return null;

        for(Format format: Format.values()){
            if(format.getFormat().length() == str.length()){
                return format.getFormat();
            }
        }
        return null;
    }
    public static Date toDate(String str,String format){
        SimpleDateFormat dateFormat = getDateFormat(format);
        if(dateFormat == null){
            return null;
        }
        try{
            return dateFormat.parse(str);
        }catch (Exception e){
            return null;
        }
    }

    public static Date toDate(String str){
        return toDate(str,getFormat(str));
    }

    public static String toStr(Date date,String format){
        if(date == null){
            return null;
        }
        return getDateFormat(format).format(date);
    }
    public static String toStr(Date date){
        return toStr(date, Format.YYYY_MM_DD_HH_MM_SS.format);
    }
}
