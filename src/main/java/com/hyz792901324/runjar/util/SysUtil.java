package com.hyz792901324.runjar.util;

import cn.hutool.core.util.RuntimeUtil;
import cn.hutool.system.SystemUtil;

/**
 * @author hyz
 * @date 2019/11/29 14:06
 */
public class SysUtil {
    public static String kill(String pid){
        String cmd = null;
        if(isWindows()){
            cmd = "taskkill /f /pid " + pid;
        }else{
            cmd = "kill -9 " + pid;
        }
        return RuntimeUtil.execForStr(cmd);
    }

    public static boolean isWindows(){
        String name = SystemUtil.getOperatingSystemMXBean().getName();
        return name.contains("Windows");
    }
}
