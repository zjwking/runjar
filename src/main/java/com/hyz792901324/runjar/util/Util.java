package com.hyz792901324.runjar.util;


import java.io.ByteArrayInputStream;
import java.io.InputStream;

public class Util {
    public static <T> T get(T t, T defaultValue) {
        return t != null && !"".equals(t) ? t : defaultValue;
    }

    public static InputStream getStringStream(String sInputString) {

        if (sInputString != null && !sInputString.trim().equals("")) {

            try {

                ByteArrayInputStream tInputStringStream = new ByteArrayInputStream(sInputString.getBytes());

                return tInputStringStream;

            } catch (Exception ex) {

                ex.printStackTrace();

            }

        }

        return null;

    }
}
