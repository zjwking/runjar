package com.hyz792901324.runjar.util;

import com.hyz792901324.runjar.rest.Resp;
import com.hyz792901324.runjar.rest.constants.StatusCode;

public class RespUtil {
    public static Resp success(){
        return success(null);
    }

    public static Resp success(Object data){
        return new Resp(StatusCode.SUCCESS,data,null);
    }


    public static Resp fail(int statusCode,String errMsg){
        return new Resp(statusCode,null,errMsg);
    }
    public static Resp fail(String errMsg){
        return fail(StatusCode.FAIL,errMsg);
    }
}
