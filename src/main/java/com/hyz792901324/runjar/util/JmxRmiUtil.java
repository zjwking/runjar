package com.hyz792901324.runjar.util;

import com.hyz792901324.runjar.admin.model.JmxRmiInfo;

import javax.management.MBeanServerConnection;
import javax.management.remote.JMXConnector;
import javax.management.remote.JMXConnectorFactory;
import javax.management.remote.JMXServiceURL;
import java.lang.management.*;
import java.util.HashMap;
import java.util.Map;

/**
 * @author hyz
 * @date 2019/11/29 15:41
 */
public class JmxRmiUtil {
    public static JmxRmiInfo getJmxRmiInfo(String ip,String port){
        try{
            String jmxURL = "service:jmx:rmi:///jndi/rmi://" + ip + ":" + port+ "/jmxrmi";
            JMXServiceURL serviceURL = new JMXServiceURL(jmxURL);

            Map map = new HashMap();
            String[] credentials = new String[] { "monitorRole", "QED" };
            map.put("jmx.remote.credentials", credentials);
            JMXConnector connector = JMXConnectorFactory.connect(serviceURL, map);
            MBeanServerConnection mbsc = connector.getMBeanServerConnection();

            MemoryMXBean mmxb = ManagementFactory.newPlatformMXBeanProxy(mbsc,"java.lang:type=Memory",
                    MemoryMXBean.class);

            RuntimeMXBean rmxb = ManagementFactory.newPlatformMXBeanProxy(mbsc,"java.lang:type=Runtime",
                    RuntimeMXBean.class);

            ThreadMXBean thxb = ManagementFactory.newPlatformMXBeanProxy(mbsc,"java.lang:type=Threading",
                    ThreadMXBean.class);

            OperatingSystemMXBean osxb = ManagementFactory.newPlatformMXBeanProxy(mbsc,"java.lang:type=OperatingSystem",
                    OperatingSystemMXBean.class);

            JmxRmiInfo jmxRmiInfo = new JmxRmiInfo();
            jmxRmiInfo.setMemoryMXBean(mmxb);
            jmxRmiInfo.setOperatingSystemMXBean(osxb);
            jmxRmiInfo.setRuntimeMXBean(rmxb);
            jmxRmiInfo.setThreadMXBean(thxb);
            //connector.close();
            return jmxRmiInfo;
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

}
