package com.hyz792901324.runjar.logger;

import com.hyz792901324.runjar.admin.model.ProjectInstance;
import com.hyz792901324.runjar.common.SpringContext;
import com.hyz792901324.runjar.data.DataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Map;

import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

@ServerEndpoint("/log")
@Component
public class LogWebSocketHandle {

    private Process process;
    private InputStream inputStream;

    /**
     * 新的WebSocket请求开启
     */
    @OnOpen
    public void onOpen(Session session) {
        try {
            Map<String, List<String>> param = session.getRequestParameterMap();
           String id = param.get("id").get(0);
            DataService dataService = SpringContext.getBean(DataService.class);
            ProjectInstance instance = dataService.findProjectInstanceById(id);

            // 执行tail -f命令
            process = Runtime.getRuntime().exec("tail -f "+instance.getLogPath());
            inputStream = process.getInputStream();

            // 一定要启动新的线程，防止InputStream阻塞处理WebSocket的线程
            TailLogThread thread = new TailLogThread(inputStream, session);
            thread.start();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    /**
     * WebSocket请求关闭
     */
    @OnClose
    public void onClose() {
        try {
            if(inputStream != null)
                inputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        if(process != null)
            process.destroy();
    }

    @OnError
    public void onError(Throwable thr) {
        thr.printStackTrace();
    }
}