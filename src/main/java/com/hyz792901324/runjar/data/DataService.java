package com.hyz792901324.runjar.data;

import com.hyz792901324.runjar.admin.model.Data;
import com.hyz792901324.runjar.admin.model.Project;
import com.hyz792901324.runjar.admin.model.ProjectInstance;
import com.hyz792901324.runjar.admin.model.ProjectVersion;

import java.io.File;
import java.util.List;

/**
 * @author hyz
 * @date 2019/11/28 11:20
 */
public interface DataService {
    void saveProject(Project project);
    void saveProject(String id,String name);
    boolean isRepeat(String id,String name);
    void deleteProject(Project project);
    void deleteVersion(String id);
    boolean isRunVersion(String id);
    ProjectVersion findVersion(String id);
    Project findProjectById(String id);
    Project findProjectByInstanceId(String id);
    ProjectInstance findProjectInstanceById(String id);
    List<Project> list();
    void loadData();
    File getFolder();
    void saveData();
    void startInstance(ProjectInstance instance);
    void stopInstance(ProjectInstance instance);
}
