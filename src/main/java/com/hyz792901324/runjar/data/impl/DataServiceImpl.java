package com.hyz792901324.runjar.data.impl;

import cn.hutool.core.io.FileUtil;
import cn.hutool.core.io.IoUtil;
import cn.hutool.core.io.LineHandler;
import cn.hutool.core.net.NetUtil;
import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.RuntimeUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.cron.CronUtil;
import cn.hutool.json.JSONUtil;
import com.hyz792901324.runjar.admin.model.*;
import com.hyz792901324.runjar.config.Config;
import com.hyz792901324.runjar.data.DataService;
import com.hyz792901324.runjar.util.DateUtil;
import com.hyz792901324.runjar.util.JmxRmiUtil;
import com.hyz792901324.runjar.util.SysUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author hyz
 * @date 2019/11/28 11:22
 */
@Service
public class DataServiceImpl implements DataService {
    private Data data = new Data();

    private static final String configFileName = "config.json";

    @Autowired
    private Config config;
    @Override
    public void saveProject(Project project) {
        if(StrUtil.isBlank(project.getId())){
            project.setId(IdUtil.fastSimpleUUID());
            data.getProjects().add(project);
        }
        this.saveData();
    }

    @Override
    public void saveProject(String id, String name) {
        Project project = null;
        if(StrUtil.isBlank(id)){
            project = new Project();
            project.setName(name);
            new File(getFolder(),name).mkdir();
        }else{
            project = findProjectById(id);
            new File(getFolder(),project.getName()).renameTo(new File(getFolder(),name));
            project.setName(name);
        }
        this.saveProject(project);
    }

    @Override
    public boolean isRepeat(String id, String name) {
        for (Project project:data.getProjects()){
            if(!project.getId().equals(id) && project.getName().equals(name)){
                return true;
            }
        }
        return false;
    }

    @Override
    public void deleteProject(Project project) {
        data.getProjects().remove(project);
        this.saveData();
    }

    @Override
    public void deleteVersion(String id) {
        for(Project project:data.getProjects()){
            for(ProjectVersion version:project.getVersions()){
                if(version.getId().equals(id)){
                    File parent =new File(getFolder(),project.getName()+"/"+version.getVersion()+"/");
                    File file = new File(parent,version.getJarPath());
                    file.delete();
                    parent.delete();

                    project.getVersions().remove(version);
                    saveProject(project);
                    return;
                }
            }
        }
    }

    @Override
    public Project findProjectById(String id) {
        for(Project project:data.getProjects()){
            if(project.getId().equals(id)){
                return project;
            }
        }
        return null;
    }

    @Override
    public List<Project> list() {
        return data.getProjects();
    }
    @Override
    public void saveData(){
        File configFile = new File(getFolder(),configFileName);
        String json = JSONUtil.toJsonPrettyStr(this.data);
        FileUtil.writeUtf8String(json,configFile);
    }
    @Override
    public void loadData() {
        String name = "config.json";
        File configFile = new File(getFolder(),configFileName);
        if(!configFile.exists()){
            String json = JSONUtil.toJsonStr(this.data);
            FileUtil.writeUtf8String(json,configFile);
        }else{
            String json = FileUtil.readUtf8String(configFile);
            this.data = JSONUtil.toBean(json, Data.class);
        }
    }
    @Override
    public File getFolder(){
        return new File(config.getPath());
    }

    @Override
    public ProjectInstance findProjectInstanceById(String id) {
        for(Project project:data.getProjects()){
            for(ProjectInstance instance:project.getInstances()){
                if(instance.getId().equals(id)){
                    return instance;
                }
            }
        }
        return null;
    }

    @Override
    public Project findProjectByInstanceId(String id) {
        for(Project project:data.getProjects()){
            for(ProjectInstance instance:project.getInstances()){
                if(instance.getId().equals(id)){
                    return project;
                }
            }
        }
        return null;
    }

    @Override
    public ProjectVersion findVersion(String id) {
        for(Project project:data.getProjects()){
            for(ProjectVersion version:project.getVersions()){
                if(version.getId().equals(id)){
                    return version;
                }
            }
        }
        return null;
    }

    @Override
    public boolean isRunVersion(String id) {
        for(Project project:data.getProjects()){
            for(ProjectInstance instance:project.getInstances()){
                if(instance.isRun()
                        && instance.getVersion() == null
                        && instance.getVersion().getId().equals(id)){
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    public void startInstance(ProjectInstance instance) {
        if(instance.isRun()){
            return;
        }
        int minPort = 20000;
        int maxPort = 60000;
        int port = minPort;
        for(port = minPort;port<=maxPort;port++){
            if(NetUtil.isUsableLocalPort(port)){
                break;
            }
        }

        instance.setJmxPort(port);
        instance.setRun(true);
        Project project = findProjectByInstanceId(instance.getId());

        if(instance.isNewVersion()){
            instance.setVersion(project.getVersions().get(project.getVersions().size()-1));
        }

        int jmxPort = port;
        String jarPath = new File(getFolder(),project.getName()+"/"+instance.getVersion().getVersion()+"/" + instance.getVersion().getJarPath()).getAbsolutePath();
        String ip = "127.0.0.1";
        String logPath = getLog(project,instance).getAbsolutePath();
        instance.setLogPath(logPath);
        String options =
                "java -Djava.rmi.server.hostname="+ip+" " +
                " -Dcom.sun.management.jmxremote " +
                " -Dcom.sun.management.jmxremote.port="+jmxPort+" " +
                " -Dcom.sun.management.jmxremote.authenticate=false " +
                " -Dcom.sun.management.jmxremote.ssl=false " +
                " -jar " + jarPath + " >> " + logPath;
//        options = "";
        String[] cmd = new String[3];
        if(SysUtil.isWindows()){
            cmd[0] = "cmd";
            cmd[1] = "/C";
            cmd[2] = options;
        }else{
            cmd[0] = "sh";
            cmd[1] = "-c";
            cmd[2] = options;
        }

        Process process = RuntimeUtil.exec(cmd);
        InputStream in = process.getInputStream();

        String pre = project.getName() + "\t" + instance.getVersion().getVersion() + "\t";
        LineHandler lineHandler = new LineHandler() {
            @Override
            public void handle(String s) {
                System.out.println(pre + s);
            }
        };
        new Thread(new Runnable() {
            @Override
            public void run() {
                IoUtil.readUtf8Lines(in, lineHandler);
            }
        }).start();
        JmxRmiInfo jmxRmiInfo = JmxRmiUtil.getJmxRmiInfo(ip, jmxPort + "");
        instance.setPid(jmxRmiInfo.getPid());
        processMap.put(jmxRmiInfo.getPid(),process);

        this.saveData();
    }

    private File getLog(Project project,ProjectInstance instance){
        File log = new File(getFolder(),"logs");
        log.mkdir();
        File projectLog = new File(log,project.getName());
        projectLog.mkdir();
        File instanceLog = new File(projectLog,instance.getId()+"-"+ DateUtil.toStr(new Date(),"yyyyMMdd")+ ".log") ;
        if(!instanceLog.exists()){
            try {
                instanceLog.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return instanceLog.getAbsoluteFile();
    }

    private Map<String,Process> processMap = new HashMap<>();

    @Override
    public void stopInstance(ProjectInstance instance) {
        if(!instance.isRun()){
            return;
        }
        Process process = processMap.get(instance.getPid());
        if(process != null){
            process.destroy();
            processMap.remove(instance.getPid());
        }
        SysUtil.kill(instance.getPid());

        instance.setRun(false);
        instance.setPid(null);
        instance.setJmxPort(0);
        if(instance.isNewVersion()){
            instance.setVersion(null);
        }
        this.saveData();
    }
}
